import { Injectable } from '@angular/core';

@Injectable() 
export class YatzeeAi {    
    constructor() {
        
    }    
    getBestUsage(role: string) {

        switch(role) { 
            case "6-6-6-6-6":
            case "5-5-5-5-5":
            case "4-4-4-4-4":
            case "3-3-3-3-3":
            case "2-2-2-2-2":
            case "1-1-1-1-1": { 
                return "yatzee";
            }
            default: {
                return "chance";
            }
        }
    }    
}

//let yatzeeAi = new YatzeeAi();