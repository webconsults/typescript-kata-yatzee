import { Component } from "@angular/core";

@Component({
    selector: "yatzee-hello",
    styleUrls: ["../assets/style/main.css", "../assets/style/main.less", "../assets/style/main.scss"],
    templateUrl: "yatzee.html"
})
export class YatzeeComponent {
    public title = "Yatzee :)";
}
