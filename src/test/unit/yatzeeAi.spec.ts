// Tutorial: https://journal.artfuldev.com/write-tests-for-typescript-projects-with-mocha-and-chai-in-typescript-86e053bdb2b6


import 'core-js/es7/reflect';
import 'mocha';
import * as Chai from 'chai';
let expect = Chai.expect;

import { YatzeeAi } from "../../app/yatzee.ai";


// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
import 'mocha';


describe('Yatzee ', () => {
  it('should return Yatzee on only 5s', () => {
          
         let yatzeeAi = new YatzeeAi();
          let role =  "5-5-5-5-5";
          let result:string = yatzeeAi.getBestUsage(role); 
    
    expect(result).to.equal('yatzee');
  });
  it('should return chance on something', () => { 
    let yatzeeAi = new YatzeeAi();
    let role = "4-3-2-1-2"; 
    let result:string = yatzeeAi.getBestUsage(role);
  
    expect(result).to.equal('chance');
  });
});