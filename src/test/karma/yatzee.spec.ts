import { DebugElement } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";

import { YatzeeComponent } from "../../app/yatzee";

describe("YatzeeComponent", () => {

    let fixture: ComponentFixture<YatzeeComponent>;

    beforeEach(async(() => {

        return TestBed
            .configureTestingModule({
                declarations: [YatzeeComponent]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(YatzeeComponent);
            });
    }));

    it("should display original title", () => {

        let debugElement = fixture.debugElement.query(By.css("h1"));
        fixture.detectChanges();

        expect(debugElement.nativeElement.textContent).toEqual("Yatzee :)");
    });

    it("should display a different test title", () => {

        let debugElement = fixture.debugElement.query(By.css("h1"));

        fixture.componentInstance.title = "Test Title";
        fixture.detectChanges();

        expect(debugElement.nativeElement.textContent).toEqual("Test Title");
    });
});
